import { Observable } from 'rxjs'
import { Div, DomComponent, DomContent, DomElement, H3 } from './dom'

export const createCard = (cl: string, cardContent: DomContent) => {
  const card = new Div({ class: ['card', cl].join(' ') })
  const content = typeof cardContent == 'function' ? cardContent(card) :
    cardContent instanceof DomComponent ? cardContent.render(card) :
      cardContent
  card.add(new Div({ class: "section" }, content))
  return card
}

export const createSmallCard = (cl: string, cardContent: DomContent) => {
  return createCard([cl, "small"].join(' '), cardContent)
}

export const createFluidCard = (cl: string, cardContent: DomContent) => {
  return createCard([cl, "fluid"].join(' '), cardContent)
}

export class FluidCard extends DomComponent {
  constructor(private cl: string, private cardContent: DomComponent | DomElement) {
    super()
  }

  render(parentElement: DomElement = null) {
    return createFluidCard(this.cl, this.cardContent)
  }
}

export const row = (rowContent: DomContent = []) => {
  return new Div({ class: 'row' }, rowContent)
}

export const col = (cl: string = "sm-3", colContent: DomContent = []) => {
  return new Div({ class: 'col-' + cl }, colContent)
}

export const h3 = (text: string | Observable<string>) => new H3({}, text)
