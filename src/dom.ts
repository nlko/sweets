import { fromEvent, Observable, Subject } from 'rxjs'
import { filter, map, scan, startWith, takeUntil, tap } from 'rxjs/operators'


type ElementAttrbs = Object | Observable<Object>

export enum InsertMethod {
  BeforeBegin = "beforebegin",
  BeforeEnd = "beforeend",
  AfterEnd = "afterend"
}

export abstract class DomComponent {
  abstract render(parentComponent: DomElement): DomContent
}

type DomTypes = number | string | DomElement | Observable<DomElement> | DomComponent | ((domElement: DomElement) => DomContent)

type RealDomTypes = DomTypes | DomTypes[]

export type DomContent = RealDomTypes | Observable<RealDomTypes>

type PostFn = ((domElement: DomElement) => void)

class SubjectOnDemand<T> {
  private subject$ = null

  get sub$(): Subject<T> {
    if (!this.subject$)
      this.subject$ = new Subject<T>()
    return this.subject$
  }
}

export class DomElement {

  onNodeRemoved$: Observable<any> = null

  private setupNodeRemovedDetector() {
    this.onNodeRemoved$ = fromEvent(this.domElement, 'DOMNodeRemoved')
    console.log('Setup node to be removed on parent removal event')

    // On node removed
    this.onNodeRemoved$.pipe(
        filter((x: any) => x.originalTarget && x.originalTarget.__SWEET_REF),
        map((x: any) => x.originalTarget.__SWEET_REF)
      ).subscribe((x: DomElement) => {
        console.log('Parent node removed')
        // trigger the proper destroy event
        x.onDestroy.sub$.next()
      })
  }

  private getSubject<T>(name: string) {
    console.log('getSubject')
    if (!this.onNodeRemoved$)
      this.setupNodeRemovedDetector()
    if (!this["_" + name]) {
      console.log('Creating subject ' + name)
      this["_" + name] = new Subject<T>()
    }
    return this["_" + name]
  }

  onDestroy = new SubjectOnDemand<void>()

  // Ensure that event are unsubscribed on destroy
  obs<T>(obs$: Observable<T>): Observable<T> {
    return obs$.pipe(takeUntil(this.onDestroy.sub$))
  }

  get add$(): Subject<DomContent> {
    const addSubject = this.getSubject<DomContent>('add$')

    this.obs(addSubject).subscribe(this.add, (err) => console.dir(err), () => console.log("Complete on add$"))

    return addSubject
  }

  get set$(): Subject<DomContent> {
    const set = this.getSubject<DomContent>('set$')

    this.obs(set).subscribe((x: any) => this.set(x), (err) => console.dir(err), () => console.log("Complete on set$"))

    return set
  }

  constructor(protected domElement: Element, innerData: DomContent = null, postFn: PostFn = null) {
    domElement['__SWEET_REF'] = this

    if (innerData !== null) {
      this.set(innerData)
    }

    if (postFn !== null) {
      postFn(this)
    }
  }

  set innerHTML(html: string) {
    this.domElement.innerHTML = html
  }

  set(data: DomContent = null) {
    this.innerHTML = null
    this.add(data, true)
  }

  addAnElement(data: DomTypes = null, isFromSet = false) {
    if (data === null) {
      this.insertAdjacentText(InsertMethod.BeforeEnd, "NULL VALUE TO INSERT")
    } else if (Array.isArray(data)) {
      data.forEach(element => this.add(element, isFromSet))
    } else if (typeof data === 'string') {
      this.insertAdjacentText(InsertMethod.BeforeEnd, data)
    } else if (typeof data === 'number') {
      this.insertAdjacentText(InsertMethod.BeforeEnd, `${data}`)
    } else if (data instanceof DomComponent) {
      this.add((<DomComponent>data).render(this))
    } else if (data instanceof DomElement) {
      this.insertAdjacentElement(InsertMethod.BeforeEnd, data)
    } else if ((typeof data === "object") && ((data as Observable<any>)._isScalar !== undefined)) {
      const obs = data as Observable<any>
      console.log('add')

      this.obs(obs).pipe(
        startWith(new DomElement(document.createElement('div'))),
        tap(_ => console.log('Add 1 Element')),
        scan((lastElement: Element, newDomElement: DomElement) => {

          // console.dir(lastElement)
          // console.dir(newDomElement)

          if (lastElement) {
            lastElement.parentNode.replaceChild(newDomElement.domElement, lastElement)
          } else {
            this.addAnElement(newDomElement, isFromSet)
          }
          return newDomElement.domElement
        }, null),
      ).subscribe((data: Element) => { },
        (err) => console.dir(err),
        () => console.log('Complete : Add 1 element'))
    } else {
      // this.innerHTML = ""
      this.insertAdjacentText(InsertMethod.BeforeEnd, "UNKNOWN VALUE TO INSERT " + data)
    }
  }

  add(data: DomContent = null, isFromSet = false) {
    if (Array.isArray(data)) {
      data.forEach((element: DomContent) => this.add(<DomContent>element, isFromSet))
    } else if (typeof data === 'function') {
      const newData: DomContent = data(this)
      if (typeof newData !== 'function')
        this.add(newData, isFromSet)
      else
        this.addAnElement("Invalid function return", isFromSet)
    } else if ((typeof data === "object") && ((data as Observable<DomContent>)._isScalar !== undefined)) {
      const obs = data as Observable<DomContent>
      this.obs(obs).subscribe(
        (dom: DomContent) => {
          if (isFromSet)
            this.set(dom)
          else
            this.add(dom, false)
        },
        (err) => console.dir(err),
        () => console.log('Complete : Add'))
    } else {
      // this.innerHTML = ""
      this.addAnElement(<DomTypes>data)
    }
  }

  insertAdjacentElement(method: InsertMethod, element: DomElement) {
    this.domElement.insertAdjacentElement(method, element.domElement)
  }

  insertAdjacentText(method: InsertMethod, text: string) {
    this.domElement.insertAdjacentText(method, text)
  }

  removeChild(element: DomElement) {
    this.domElement.removeChild(element.domElement)
  }

  remove() {
    this.domElement.parentNode.removeChild(this.domElement)
  }

  onEvent(event: string) {
    return this.obs(fromEvent(this.domElement, event, (evt: any) => evt))
  }

  setAttrb = (key: string, value: string) => this.domElement.setAttribute(key, value)
  removeAttrb = (key: string) => this.domElement.removeAttribute(key)
}

export class Elem extends DomElement {
  constructor(elementType: string, attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    const element: Element = document.createElement(elementType)

    super(element, innerData, postFn)

    if (attrbs) {
      this.manageAttrbs(attrbs)
    }
  }

  private manageAttrbs(attrbs: ElementAttrbs) {
    // Set an setattribute
    const setAttrb = (key: string, value: string) => this.domElement.setAttribute(key, value)

    // Remove an attribute
    const removeAttrb = (key: string) => this.domElement.removeAttribute(key)

    // Set an attribute or remove it if the value is null
    const processAttrChange = ({ key, value }: { key: string, value?: string }) =>
      value !== undefined ? (value !== undefined ? setAttrb(key, value) : removeAttrb(key)
      ) : null

    // const setRxAttrb = (attrb$: Observable<{ key: string, value?: string }>) => this.obs(attrb$).subscribe(processAttrChange)

    const setAttrbs = (attrbs: Object) =>
      Object.keys(attrbs).forEach(key => {
        const value = attrbs[key]
        if (value === undefined) {
          removeAttrb(key)
        } else if (typeof value === "string") {
          processAttrChange({ key, value })
        } else if ((typeof value === "object") && ((value as Observable<string>)._isScalar !== undefined)) {
          this.obs(value as Observable<string>).subscribe((value: string) => processAttrChange({ key, value }))
        }
      })

    if ((typeof attrbs === "object") && ((attrbs as Observable<Object>)._isScalar === undefined)) {
      setAttrbs(<Object>attrbs)
    } else if ((typeof attrbs === "object") && ((attrbs as Observable<Object>)._isScalar !== undefined)) {
      this['rxAttrb'] = this.obs(<Observable<Object>>attrbs)
      this['rxAttrb'].subscribe((attrbs: Object) => this.manageAttrbs(attrbs))
    }
  }

}

export class Button extends Elem {
  onClick$: Observable<any>
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: (_: Button) => void = null) {
    super('button', attrbs, innerData)
    this.onClick$ = this.onEvent('click')
    if (postFn) postFn(this)
  }

  onClick = (onClickCb: (evt: any) => void): void => { this.onClick$.subscribe(onClickCb) }
}

export const SimpleButton = (attrbs: ElementAttrbs = null, innerData: DomContent = null, onClickCb: (evt: any) => void): Button =>
  new Button(attrbs, innerData, (self) => {
    self.onClick$.subscribe(onClickCb)
  })

export class Input extends Elem {
  onKeydown$: Observable<any>
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: (_: Input) => void = null) {
    super('input', attrbs, innerData)
    this.onKeydown$ = this.onEvent('keydown')
    if (postFn) postFn(this)
  }

  onKeydown = (onKeydownCb: (evt: any) => void): void => { this.onKeydown$.subscribe(onKeydownCb) }
}

export class P extends Elem {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('p', attrbs, innerData, postFn)
  }
}

export class Container extends Elem {
  constructor(type: string = 'div', attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super(type, attrbs, innerData, postFn)
  }
}

export class Div extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('div', attrbs, innerData, postFn)
  }
}

export class Ul extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('ul', attrbs, innerData, postFn)
  }
}

export class Li extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('li', attrbs, innerData, postFn)
  }
}


export class Table extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('table', attrbs, innerData, postFn)
  }
}

export class Tbody extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('tbody', attrbs, innerData, postFn)
  }
}

export class Tr extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('tr', attrbs, innerData, postFn)
  }
}

export class Td extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('td', attrbs, innerData, postFn)
  }
}

export class A extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('a', attrbs, innerData, postFn)
  }
}

export class Label extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('label', attrbs, innerData, postFn)
  }
}

export class Form extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('form', attrbs, innerData, postFn)
  }
}

export class H1 extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('h1', attrbs, innerData, postFn)
  }
}

export class H2 extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('h2', attrbs, innerData, postFn)
  }
}

export class H3 extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('h3', attrbs, innerData, postFn)
  }
}

export class H4 extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('h4', attrbs, innerData, postFn)
  }
}

export class H5 extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('h5', attrbs, innerData, postFn)
  }
}

export class H6 extends Container {
  constructor(attrbs: ElementAttrbs = null, innerData: DomContent = null, postFn: PostFn = null) {
    super('h6', attrbs, innerData, postFn)
  }
}
