import { DomList } from './domlist'
import { DomElement, DomComponent, P } from './dom'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'


export default class CardCounter extends DomComponent {
  constructor(private number$: Observable<number>) {
    super()
  }

  render() {
    return new P({}, this.number$.pipe(
      map((nb: number): string => {
        return nb === 0 ? "No card" : nb === 1 ? "1 card" : "" + nb + " cards"
      })
    ))
  }
}
