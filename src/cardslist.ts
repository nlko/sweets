import { Subject } from 'rxjs'
import CardContent from './cardcontent'
import { DomComponent } from './dom'
import { DomList } from './domlist'
import InputCard from './inputcard'
import { col, FluidCard, row } from './minicss_helpers'
import { uuidv4 } from './uuidv4'
import { Store } from 'storyx'
import { take, switchMap, map } from 'rxjs/operators'


export default class CardsList extends DomComponent {

  public list$ = this.store
    .select$('cardsList').pipe(
      take(1),
      switchMap((cardsList: DomList) => cardsList.obs$)
    )

  public addCardAction$ = new Subject<string>()

  private displayCards = col('md-12', [new InputCard(this.addCardAction$)])
  private toDisplay = row(this.displayCards)

  constructor(private store: Store) {
    super()

    this.addCardAction$.subscribe(this.addCardAction)
    this.store.update('cardsList', new DomList(this.displayCards, []))
  }

  addCardAction = (text: string) => {
    this.store
      .select$('cardsList').pipe(
        take(1),
        map((cardsList: DomList) => {
          const newId = uuidv4()
          const domElement = new FluidCard("", new CardContent(cardsList, newId, text)).render(null)
          domElement['_id'] = newId
          return {
            cardsList,
            domElement
          }
        })
      ).subscribe(({ cardsList, domElement }) => {
        cardsList.add(domElement)
      })
  }

  render() {
    return this.toDisplay
  }
}
