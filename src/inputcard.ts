import { Subject } from 'rxjs'
import { DomComponent, DomElement, DomContent, Input } from './dom'
import { createFluidCard } from './minicss_helpers'
import { map, filter } from 'rxjs/operators'

export default class InputCard extends DomComponent {

  input: Input

  constructor(addCard$: Subject<string>) {
    super()

    this.input = new Input({}, 'test',
      (self) => {
        self.onKeydown$.pipe(
          filter(key => 13 === key.keyCode),
          map(x => x.target.value)
        ).subscribe(addCard$)
      })
  }

  render(): DomContent {
    return createFluidCard('', (cardElement: DomElement) => (self: DomElement) => this.input)
  }
}
