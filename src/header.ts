import { interval } from 'rxjs'
import { scan } from 'rxjs/operators'
import { A, DomElement, Elem } from './dom'


export default function myHeader(title: string): DomElement  {

  const headerText$ = interval(200).pipe(
    scan((acc: string) => {
      return acc.slice(1) + acc[0]
    }, title))

  return new Elem('header', { class: "sticky" }, (self) => [
    new A({ href: "#", class: "logo" }, headerText$)
  ])
}
