import { Subject } from 'rxjs'
import { DomElement, DomComponent, SimpleButton } from './dom'

export default class AddCardButton extends DomComponent {
  constructor(private addCardAction$: Subject<string>) {
    super()
  }

  render(parentComponent: DomElement = null) {
    return SimpleButton({}, 'add', _ => {
      this.addCardAction$.next('hello')
    })
  }
}
