import { DomElement, DomContent, DomComponent, Elem, Button, Div, P, Li, Ul, Td, Table, Tr, Tbody, A, Input, Label, Form, H3, SimpleButton } from './dom'
import { row } from './minicss_helpers'
import myHeader from './header'
import CardCounter from './cardcounter'
import AddCardButton from './addcardbutton'
import RollBackButton from './rollbackbutton'
import CardsList from './cardslist'
import { Store } from 'storyx'
import { map } from 'rxjs/operators'

const store = new Store()

const app = document.getElementById('app')

const App = new DomElement(app, (self: DomElement): DomContent => {

  const cardsList = new CardsList(store)

  const addCardButton = new AddCardButton(cardsList.addCardAction$)

  return [
    myHeader("-SweeTask-"),
    addCardButton,
    new CardCounter(cardsList.list$.pipe(
      map((lst) => lst.length)
    )),
    cardsList,
    addCardButton,
    new RollBackButton(store)
  ]
})
