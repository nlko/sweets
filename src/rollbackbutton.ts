import { DomComponent, DomElement, SimpleButton } from './dom'
import { Store } from 'storyx'

export default class RollBackButton extends DomComponent {
  constructor(private store: Store) {
    super()
  }

  render(parentElement: DomElement) {
    return SimpleButton({}, 'Rollback', _ => {
      this.store.rollback()
      this.store.dump()
    })
  }
}
