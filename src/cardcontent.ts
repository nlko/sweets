import { DomComponent, DomElement, SimpleButton } from './dom'
import { DomList } from './domlist'
import { FluidCard, col, row, h3 } from './minicss_helpers'
import { BehaviorSubject } from 'rxjs'

export default class CardContent extends DomComponent {
  constructor(private cardsList: DomList, private uuid: string, private text: string) {
    super()
  }

  render(cardElement: DomElement) {

    const cardDeleteButton = (uuid: string, cardElement: DomElement): DomElement =>
      SimpleButton({}, 'X', _ => {
        this.cardsList.removeWithID(uuid)
      })

    return (self: DomElement) => row((self) => {
      const text$ = new BehaviorSubject<string>(this.text + ' ' + this.uuid)

      return [
        col('sm-3', cardDeleteButton(this.uuid, cardElement)),
        col('sm-6', h3(text$)),
        col('sm-3', SimpleButton({}, 'change', _ => text$.next('changed')))
      ]
    })
  }
}
