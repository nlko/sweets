export let startMeasure = function(name: string) {
  const startTime = performance.now();
  const lastMeasure = name;
  return { startTime, lastMeasure }
}
export let stopMeasure = function({ startTime, lastMeasure }) {
  var last = lastMeasure;
  if (lastMeasure) {
    window.setTimeout(function() {
      lastMeasure = null;
      var stop = performance.now();
      var duration = 0;
      console.log(last + " took " + (stop - startTime));
    }, 0);
  }
}
