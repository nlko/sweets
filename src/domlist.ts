import { ObjList } from 'storyx'
import { DomElement } from './dom'
import { uuidv4 } from './uuidv4'
import { map, filter, take } from 'rxjs/operators'
import { Observable } from 'rxjs'

export class DomList<ID= string> extends ObjList<DomElement, ID> {
  constructor(
    private parentElement: DomElement,
    initialContent: DomElement[] = [],
    protected idField = "_id",
    idGenerator = uuidv4) {
    super(initialContent, idField, idGenerator)
  }

  removeWithID(id: ID): void {
    this.obs$.pipe(
      map(list => list.find(elem => elem[this.idField] === id)),
      filter(elem => !!elem),
      take(1)
    ).subscribe((elemToRemove: DomElement) => {
        elemToRemove.remove()
        super.remove(elemToRemove[this.idField])
      })
  }

  add(element: DomElement): Observable<number> {
    this.parentElement.add(element)
    return this.add$1(element)
  }
}
